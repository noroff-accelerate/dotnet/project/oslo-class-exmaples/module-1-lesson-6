using System;
using Xunit;
using TestingExample;

namespace CalculatorTester
{
    public class Tests
    {
        // Fact for a single test
        [Fact]
        public void ShouldSum()
        {
            // Arrange
            double expected = 10;
            // Act
            double actual = Calculator.Add(5, 5);
            // Assert
            Assert.Equal(expected, actual);
        }

        // Thoery for testing mutiple values for the same method
        [Theory]
        // Arrange
        [InlineData(4,2,6)]
        [InlineData(-70,45,-25)]
        [InlineData(-10,-25,-35)]
        public void ShouldSumTheory(double num1, double num2, double expected)
        {
            // Act
            double actual = Calculator.Add(num1, num2);
            // Assert
            Assert.Equal(expected, actual);
        }

        // Fact for testing for divide by zero
        [Fact]
        public void ShouldThrowDivideByZero()
        {
            // Assert
            Assert.Throws<DivideByZeroException>(() => Calculator.Divide(5, 0));
        }
    }
}
