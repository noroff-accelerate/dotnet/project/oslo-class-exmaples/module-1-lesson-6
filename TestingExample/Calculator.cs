﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingExample
{
    public class Calculator
    {
        public static double Add(double number1, double number2)
        {
            return number1 + number2;
        }
        public static double Subtract(double number1, double number2)
        {
            return 1;
        }
        public static double Multiply(double number1, double number2)
        {
            return 1;
        }
        public static double Divide(double number1, double number2)
        {
            if (number2 == 0)
                throw new DivideByZeroException();
            else
                return number1/number2;
        }
    }
}
