﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharp_feat_ext
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Lambda
            List<int> numbers = new List<int>() { 2, 6, 34, 8, -3, 76, 5, -9, 12, 54, 23, -65, 89, -10 };
            Console.WriteLine("\nCollection");
            PrintCollection(numbers);
            #region Old, non lambda way
            // Lambda expressions replace the old way we assigned delegates
            // What we used to do was make methods to assign the to delegates
            Check EvenCheck = IsEven;
            Check OddCheck = IsOdd;
            Check PositiveCheck = IsPositive;
            Check NegativeCheck = IsNegative;
            // Print even numbers
            Console.WriteLine("\nEven numbers");
            PrintCollection(FilterMyList(numbers, IsEven));
            // Print odd numbers
            Console.WriteLine("\nOdd numbers");
            PrintCollection(FilterMyList(numbers, IsOdd));
            // Print even positive numbers
            Console.WriteLine("\nPositive even numbers");
            PrintCollection(FilterMyList(FilterMyList(numbers,IsPositive), IsEven)); // Maybe not best practices, but just showing you can do this.
            #endregion
            // With Lambda, we dont have to create those IsOdd, ect. methods. We can make them on the fly.
            // Here we create a lambda expression to check if a number is positive
            Console.WriteLine("\nPositive numbers using Lambda");
            PrintCollection(FilterMyList(numbers, n =>
                {
                    return n > 0;
                }
            ));

            #endregion

            #region LINQ
            // LINQ
            List<int> bunchOfNumbers = new List<int>(){ 23, 6, 2, 12, 87, 400, -46, -21, 54, -8, 86, -23, 76, 37, 85, 3, 45, 6 };
            // Filter list to show only negative numbers, show IEnumerable and List (.ToList)
            IEnumerable<int> negativeIEnumerable = from number in bunchOfNumbers
                                     where number < 0
                                     select number;
            List<int> negativeList = (from number in bunchOfNumbers
                                     where number < 0
                                     select number).ToList();
            // Count how many positive numbers there are
            int positiveNumbers = (from number in bunchOfNumbers
                                   where number > 0
                                   select number).Count();
            #endregion
        }

        #region Print collection method
        private static void PrintCollection<T>(IEnumerable<T> collection)
        {
            foreach(T item in collection)
                Console.Write(item.ToString() + " ");
        }
        #endregion

        #region Lambda example methods - old way
        // Declare delegate that compares two numbers and returns a bool
        public delegate bool Check(int number);
        // Create some methods to assign to delegate
        // If n1 is larger than n2 return true.
        public static bool IsEven(int number)
        {
            return number%2 == 0;
        }
        // If n1 is equal to n2 return true.
        public static bool IsOdd(int number)
        {
            return number%2 != 0;
        }
        // If n1 is less than n2 return true.
        public static bool IsNegative(int number)
        {
            return number < 0;
        }
        public static bool IsPositive(int number)
        {
            return number > 0;
        }

        // Method to use delegate
        public static List<int> FilterMyList(List<int> numbers, Check check)
        {
            // Same as previous example - we return a filtered list based on some criteria.
            List<int> filtered = new List<int>();

            foreach (int num in numbers)
                if (check(num))
                    filtered.Add(num);

            return filtered;
        }
        #endregion
    }
}
